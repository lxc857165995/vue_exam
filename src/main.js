import Vue from 'vue'
import App from './App.vue'
//1.引入文件
import first from '@/views/first'
import second from '@/views/second'
import third from '@/views/third'
import fourth from '@/views/fourth'
import fifth from '@/views/fifth'
import sixth from '@/views/sixth'
import seventh from '@/views/seventh'
import eighth from '@/views/eighth'
import ninth from '@/views/ninth'

//2.引入得到一个路由-函数对象
import VueRouter from 'vue-router'

//3.注册全局组件 注册了RouterLink 和 RouterView两个组件
Vue.use(VueRouter) 

//4.定义路由规则数组(重要)-映射规则(路径-组件名)
const routes = [
    {
        path: "/", //网页第一次打开 默认是/路由路径
        redirect: "/first"
    },
    { //第一
        path: "/first",
        component: first,

    },
    { //第二
        path: "/second",
        component: second,

    },
    { //第三
        path: "/third",
        component: third,

    },
    { //第四
        path: "/fourth",
        component: fourth,

    },
    { //第五
        path: "/fifth",
        component: fifth,

    },
    { //第六
        path: "/sixth",
        component: sixth,

    },
    { //第七
        path: "/seventh",
        component: seventh,

    },
    { //第八
        path: "/ninth",
        component: ninth,

    },
    { //第九
        path: "/eighth",
        component: eighth,

    },
]
//5.使规则数组生成路由对象
const router = new VueRouter({
    routes
})

//全局前置 - 路由守卫：在路由跳转之前,判断你是否有权限可以进入下一个路由页面
// router.beforeEach((to, from, next) => {
//     // console.log(to); //要跳转到的页面
//     // console.log(from); //从哪个页面来的
//     // console.log(next);
//     //next() //路由守卫里面使用的 next()的方法

//     let login = true //模拟未登录页面  判断是否有登陆权限，没有提示，有了跳转
//     if (to.path === "/my" && login === false) {
//         alert("给我滚去登录！");
//     } else {
//         next();  //阻止路由跳转(原地跳转 - 不写也可以)
//     }
// })


Vue.config.productionTip = false
new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
